Spray-Eicar
===========

Enumerate antivirus directory level on-access scanner exclusions and make a lot of noise.

Usage
=====
    spray-eicar.pl [-vd] [-p path] [-f file]
    Enumerate directory level antivirus exclusions.

      -d, --debug       Print details about every action taken
      -v, --verbose     Print more information about errors
      -p, --path 'PATH' Follow subdirectories of PATH, default is C:\
      -f, --file 'FILE'     Use FILE as name of EICAR test file
      -V, --version     Print eicar-spray.pl version information
    HELP